﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(knockoutjs.Startup))]
namespace knockoutjs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
