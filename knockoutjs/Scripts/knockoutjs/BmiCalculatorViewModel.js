﻿function bmiCalculatorViewModel() {

    this.gender = ko.observable("");

    // Weight, height and BMI
    this.weightMetric = ko.observable(91.00);
    this.heightMetric = ko.observable(1.87);
    this.bmiScore = ko.computed(function () {

        return (parseFloat(this.weightMetric()) / (parseFloat(this.heightMetric()) * parseFloat(this.heightMetric()))).toFixed(2);

    }, this);

    this.bmiScoreCss = ko.computed(function () {

        if (this.gender() === "Female") {
            if (this.bmiScore() < 19.1) {
                return "label-danger";
            } else
                if (this.bmiScore() <= 25.8) {
                    return "label-success";
                } else
                    if (this.bmiScore() < 27.3) {
                        return "label-warning";
                    } else {
                        return "label-danger";
                    }
        } else {
            if (this.bmiScore() < 20.7) {
                return "label-danger";
            } else
                if (this.bmiScore() <= 26.4) {
                    return "label-success";
                } else
                    if (this.bmiScore() < 27.8) {
                        return "label-warning";
                    } else {
                        return "label-danger";
                    }
        }

    }, this);

    this.bmiScoreLevel = ko.computed(function () {

        if (this.gender() === "Female") {
            if (this.bmiScore() < 19.1) {
                return "Underweight";
            } else
                if (this.bmiScore() <= 25.8) {
                    return "Normal";
                } else
                    if (this.bmiScore() < 27.3) {
                        return "Marginally Overweight";
                    } else
                        if (this.bmiScore() < 32.3) {
                            return "Overweight";
                        } else {
                            return "Obese";
                        }
        } else {
            if (this.bmiScore() < 20.7) {
                return "Underweight";
            } else
                if (this.bmiScore() <= 26.4) {
                    return "Normal";
                } else
                    if (this.bmiScore() < 27.8) {
                        return "Marginally Overweight";
                    } else
                        if (this.bmiScore() < 31.1) {
                            return "Marginally Overweight";
                        } else {
                            return "Obese";
                        }
        }
    }, this);

}