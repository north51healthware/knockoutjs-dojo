﻿describe("BMI Level for Obese Male", function () {
    it("Gives a value of Obese", function () {
        var target = new bmiCalculatorViewModel();
        target.gender("Male");
        target.weightMetric(93.0);
        target.heightMetric(1.65);
        expect(target.bmiScoreLevel()).toBe("Obese");
    });

    it("Gives a CSS value of Obese", function () {
        var target = new bmiCalculatorViewModel();
        target.gender("Male");
        target.weightMetric(93.0);
        target.heightMetric(1.65);
        expect(target.bmiScoreCss()).toBe("label-danger");
    });
});